# Read echo envelope from Airmar EchoRange+ SS510 depth sensor 

# usage : file="xxx";source("eec.R")

# License : GNU General Public License v3.0

chut <- suppressPackageStartupMessages
chut(library(R.utils, quietly=T, warn.conflicts=F))
chut(library(dplyr, quietly=T, warn.conflicts=F))

ahex2i<-function(x) {
	return(as.integer(paste0("0x",x)))
	}

ranges=c("Short (17 m)","Medium (69 m)","Long (137 m)","Very long (206 m)")
lock=c("No","Yes")
sr=c(25e-6,100e-6,200e-6,300e-6)	# Sampling rate

e<-read.table(file,header=F,sep=",",check.names=FALSE,stringsAsFactors=FALSE,fill=T,skipNul=T)
e<-filter(e,!is.na(V2))
printf("%d lignes\n",nrow(e))
t0=as.integer(e[1,2])
printf("t0 = %d\n\n",t0)

repeat {
	t<-as.double(readline(prompt = "Time (seconds) : "))
	l<-e[abs(e$V2-t0-t*1000)<100,]
	if (nrow(l)==0)
		{
		printf("Fin de fichier dépassée.\n\n")
		next
		}
	printf("Now : %s\n",date());
	printf("Timestamp\t : %s ms\n",l[2]);
	printf("Elapsed time\t : %.1f s\n",t);
	dbt=as.integer(l[3])/100
	printf("Depth\t\t : %.2f m\n",dbt)
	printf("Target used\t : %s\n",l[4])
	printf("Target integrity : %d/20\n",ahex2i(l[5]))
	printf("Floor noise\t : %d/255\n",ahex2i(l[6]))
	ms = ahex2i(l[7])		# Machine state
	printf("Lock\t\t : %s\n",lock[bitwShiftR(bitwAnd(ms,32),5)+1])
	range = bitwShiftR(bitwAnd(ms,24),3)+1
	printf("Range\t\t : %s\n",ranges[range])
	printf("Pulses per ping  : %d\n\n",bitwShiftR(bitwAnd(ms,4032),6)*8+bitwAnd(ms,7))
	
	ee<-ahex2i(l[22:920])*100/255
	x=c(0:(length(ee)-1))
	x<-1500*sr[range]*x/2
	plot(x,ee,type='l',xlab="Depth (m)",ylab="Amplitude (%)",xlim=c(0,max(x)),main=sprintf("%.3f s",(l[2]-t0)/1000))
	#plot(ee,type='b',xlab="Depth (m)",ylab="Amplitude",xlim=c(0,200),main=sprintf("%s ms",e[2]))
	abline(v=dbt,col="red")
	text(dbt+0.1,20,dbt,col="red",adj=0,cex=1.2)
	
	# Targets :
	x<-1500*sr[range]*ahex2i(l[seq(9,19,2)])/2
	y<-ahex2i(l[seq(8,18,2)])*100/255
	points(x,y,col="blue",cex=1.3)
	text(x+0.05,y+2,paste0('T',c(0:5)),col="blue",adj=0)
	grid();
	abline(v=c(1:10), col = "lightgray", lty = "dotted")
}
