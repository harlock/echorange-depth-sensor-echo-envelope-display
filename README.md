# Echo envelope from Airmar EchoRange+ SS510 depth sensor 

This R program extracts data from an echo envelope file coming from [Airmar EchoRange+ SS510](https://www.airmar.com/productdescription.html?id=112) depth sensor, and displays a graph of the echo envelope.

## Getting the envelope file

Sending the command `PAMTC,EEC,ON` to sensor's RS422 interface activate echo envelope transmission.

For example, if RS422 is connected to ttyUSB0 port of the computer, the command could be issued this way in a shell :

```bash
stty -F /dev/ttyUSB0 4800
echo -e '$PAMTC,EEC,ON*9\r\n' > /dev/ttyUSB0
```

The echo envelope is then transmitted by sensor RS485 interface at 921600 baud. If this interface is connected to ttyUSB1 port of the computer, the envelope could be recorded in the file `eec.txt` this way :

```bash
stty -F /dev/ttyUSB1 921600
cat /dev/ttyUSB1 >> eec.txt
```

## Running `eec.R` program

Inside R shell, you can run `eec.R` this way :

```R
file="eec.txt";source("eec.R")
```

The program will load the echo envelope file and ask for the time of the echo you want to display (in seconds, from the beginning of the transmission).

Then it shows information about the echo in human readable form, and displays a graph of the echo envelope :

![Echo envelope graph](eec.png)

Blue dots are targets detected by sensor's firmware, and red line is target selected as true depth.

## References
* AIRMAR (2017), EchoRange / EchoRange+ Technical Manual.


